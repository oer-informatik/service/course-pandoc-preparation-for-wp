#!/usr/bin/env python3
import argparse
import yaml
import json
import requests
import re
import os
import git
import urllib.parse
from jinja2 import Template
from urllib.parse import urlparse
from helper import init_list, get_creator_string
from datetime import datetime


# See examples in : https://stackabuse.com/command-line-arguments-in-python/

parser = argparse.ArgumentParser()
parser.add_argument('filename', help='The filename of the markdown-file without extension ".md"')
parser.add_argument('--no-license', dest='create_license', action='store_false', help='Deactivate general license hint')
parser.add_argument('--no-sources', dest='create_sources_link', action='store_false', help='Deactivate link to sources')
parser.add_argument('--no-tullu', dest='create_tullu', action='store_false', help='Deactivate generation of TULLU hint')
parser.add_argument('--project-url', dest='project_url', help='git-sources of the project')
args = parser.parse_args()

text_templates = {
    "mtullu": {
        "de": "<div about=\"{{ link }}\"><a rel=\"dc:source\" href=\"{{ image_page }}\"><span property=\"dc:title\">{{ image_title }}</span></a> von <a rel=\"cc:attributionURL dc:creator\" href=\"{{ image_author_link }}\" property=\"cc:attributionName\">{{ image_author }}</a> unter <a rel=\"license\" href=\"{{ license_url }}\">{{ license_short_name }}</a></div>",
        "en": "<div about=\"{{ link }}\"><a rel=\"dc:source\" href=\"{{ image_page }}\"><span property=\"dc:title\">{{ image_title }}</span></a> by <a rel=\"cc:attributionURL dc:creator\" href=\"{{ image_author_link }}\" property=\"cc:attributionName\">{{ image_author }}</a> under <a rel=\"license\" href=\"{{ license_url }}\">{{ license_short_name }}</a></div>"
    },
    "reusage_note": {
        "de": """
---
_**Hinweis zur Nachnutzung als Open Educational Resource (OER)**_

{%if create_license %}Dieser Artikel und seine Texte, Bilder, Grafiken, Code und sonstiger Inhalt sind - sofern nicht anders angegeben - lizenziert unter {{ course_license_short_name }}.{% endif %}
{%if create_attribution %}Nennung gemäß [TULLU-Regel](https://open-educational-resources.de/oer-tullu-regel/) bitte wie folgt: _"[{{ course_title }}]({{ course_url }})" von {{ course_author }}, Lizenz: [{{ course_license_short_name }}]({{ course_license_url }})_.{% endif %}
{%if create_sources_link %}Der Artikel wurde unter [{{ course_url }}]({{ course_url }}) veröffentlicht, die Quelltexte sind in weiterverarbeitbarer Form verfügbar im Repository unter [{{project_url}}]({{project_url}}).{% endif %}
""",
        "en": """

## Note on reuse as open educational resource (OER)

{%if create_license %}This work and its contents are licensed under {{ course_license_short_name }} unless otherwise noted.{% endif %}
{%if create_attribution %}Attribution according to [TASLL rule](https://open-educational-resources.de/wp-content/uploads/graphic_TASLL-rule_OER-2.pdf) please as follows: "[{{ course_title }}]({{ course_url }})" by {{ course_author }}, license: [{{ course_license_short_name }}]({{ course_license_url }}).{% endif %}
{%if create_sources_link %}The sources of this work are available on [{{project_url}}]({{project_url}}).{% endif %}
"""
    }
}

data = {}
with open("metadata.yml", 'r') as course_metadata:
    try:
        data = yaml.safe_load(course_metadata)
    except yaml.YAMLError as exc:
        print(exc)
        print("Cannot read metadata.yml")
        exit(1)

lngs = init_list(data, "inLanguage")
if "en" in lngs:
    output_lng = "en"
elif "EN" in lngs:
    output_lng = "en"
elif "de" in lngs:
    output_lng = "de"
elif "DE" in lngs:
    output_lng = "de"
else:
    output_lng = "en"

# find wikimedia images
course = open(args.filename + ".md", "rt")
text = course.read()
images = re.findall("!\[([^\]]*)\]\(([^\)]*)\)", text)
for treffer in images:
    description = treffer[0]
    link = treffer[1]
    if "wikimedia" in link:
        print("TREFFER : " + description + " " + link)
        image_name = re.findall("\/([^\/]*)$", link)[0]

        # get image_name metadata
        session = requests.Session()
        api_url = "https://en.wikipedia.org/w/api.php"
        params_image = {
            "action": "query",
            "format": "json",
            "prop" : "imageinfo",
            "iiprop": "user|userid|canonicaltitle|url|extmetadata",
            "titles": "File:" + image_name
        }

        image_data = session.get(url=api_url, params=params_image).json()
        # print(json.dumps(image_data, indent=4, ensure_ascii=0))

        IPAGES = image_data["query"]["pages"]

        for k, v in IPAGES.items():
            print("LIZENZ : " + v["imageinfo"][0]["extmetadata"]["LicenseShortName"]["value"])
            image_info = v["imageinfo"][0]
            image_title = image_info["extmetadata"]["ObjectName"]["value"]
            image_author = image_info["user"]
            image_author_link = "https://commons.wikimedia.org/wiki/User:" + image_author
            image_page = image_info["descriptionurl"]
            license_name = image_info["extmetadata"]["UsageTerms"]["value"]
            license_short_name = image_info["extmetadata"]["LicenseShortName"]["value"]
            if (license_short_name == "Public domain") :
                license_url = "https://creativecommons.org/publicdomain/zero/1.0/deed.de"
            else :
                license_url = image_info["extmetadata"]["LicenseUrl"]["value"]

            # machine readable TULLU string
            mtullu = Template(text_templates["mtullu"][output_lng]).render(
                link=link, image_page=image_page, image_title=image_title, image_author_link=image_author_link, image_author=image_author, license_url=license_url, license_short_name=license_short_name
            )
            # print(mtullu)

            # replace original image with image + citation
            text = re.sub("!\[" + description + "\]\(" + link + "\)", "![" + description + "](" + link + ")" + "  \n" + mtullu, text)

titletags = re.findall("## ([^\n\r\t]*)", text)
markdown_title = "" if (len(titletags) == 0) else titletags[0]

titleurl = re.findall('<span class="hidden-text" title="arcticleurl">(.*?)</span>', text)
wordpress_url = "" if (len(titleurl) == 0) else titleurl[0]

mastodonurls = re.findall('<span class="hidden-text" title="mastodonurl">(.*?)</span>', text)
mastodonurl = "" if (len(mastodonurls) == 0) else mastodonurls[0]

course_license_text = ""
if args.create_license or args.create_tullu or args.create_sources_link:
    if markdown_title == "" :
        course_title = data["name"]
    else:
        course_title = markdown_title
    if "url" in data :
        if wordpress_url == "":
            course_url = data["url"]
        else:
            course_url = wordpress_url
        domain = urlparse(course_url).netloc
    else :
        course_url = ""
        domain = ""
    course_author = ", ".join(map(lambda a: get_creator_string(a), init_list(data, 'creator')))
    # course_author_url =

    course_license_url = data["license"]
    if "public-domain" in course_license_url or "zero" in course_license_url :
        course_license_short_name = "Public domain"
    else :
        course_license_components = re.findall("licenses\/([^\/]*)\/([^\/]*)", course_license_url)
        course_license_code = course_license_components[0][0]
        course_license_version = course_license_components[0][1]
        course_license_short_name = "CC " + course_license_code.upper() + " " + course_license_version

    course_license_text = Template(text_templates["reusage_note"][output_lng]).render(
        create_license=args.create_license, create_attribution=args.create_tullu, create_sources_link=args.create_sources_link,
        course_author=course_author,
        course_license_short_name=course_license_short_name, course_license_url=course_license_url,
        course_title=course_title, 
        course_url=course_url,
        domain=domain,
        project_url=args.project_url
    )

# Heutiges Datum ersetzen, wenn der entsprechende Block gefunden wird:
last_changed = ""
repo = git.Repo("./")
tree = repo.tree()

for blob in tree:
    if blob.path == args.filename + ".md":
        commit = next(repo.iter_commits(paths=blob.path, max_count=1))
        last_changed=datetime.utcfromtimestamp(commit.committed_date).strftime('%d.%m.%Y')

if last_changed == "":
    last_changed = os.path.getmtime(args.filename + ".md")
    last_changed=datetime.utcfromtimestamp(last_changed).strftime('%d.%m.%Y')

paramtext = urllib.parse.quote(course_title+" auf "+course_url+" via @oerinformatik@bildung.social")
teilen_link ="(https://s2f.kytta.dev/?text="+paramtext+")"

if mastodonurl != "":
    comment_link= "("+str(mastodonurl)+")"
else:
    comment_link=teilen_link

comments = """

_**[\[Kommentare zum Artikel lesen, schreiben\]]"""+comment_link+" / [\[Artikel teilen\]]"+teilen_link+" /  [\[gitlab-Issue zum Artikel schreiben\]]("+str(args.project_url)+"/-/issues/new)**_"

text = re.sub("(>\ \*\*tl\/dr;\*\*\ \_.*[.!?])(\_\s)", r'\g<1>'+" (Zuletzt geändert am "+str(last_changed) + ")"+ r'\g<2>', text)

stand_text = " Stand:  "+str(last_changed) + "."

with open(args.filename + "-tagged.md", 'w', encoding='utf8') as course_tagged:
    course_tagged.write(text + course_license_text + stand_text +" "+comments)

course.close()
