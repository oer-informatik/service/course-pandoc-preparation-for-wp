#!/usr/bin/env python3

import yaml
import json
from helper import init_list, get_creator_string


def add_default_value(item, field_name, default_value):
    if field_name not in item:
        item[field_name] = default_value


with open("metadata.yml", 'r') as stream:
    try:
        data = yaml.safe_load(stream)
    except yaml.YAMLError as exc:
        print(exc)

    data['@context'] = 'http://schema.org/'
    data['publisher'] = init_list(data, 'publisher')
    data['creator'] = init_list(data, 'creator')
    for c in data['publisher']:
        add_default_value(c, '@type', 'Person')
    for c in data['creator']:
        add_default_value(c, '@type', 'Person')

    with open("metadata.json", 'w', encoding='utf8') as outputfile:
        jsonstring = json.dumps(data, indent=4, ensure_ascii=0)
        tagstring = '<link rel="license" href="' + data['license'] + '"/>'
        tagstring = tagstring + '<script type="application/ld+json">' + jsonstring + '</script>'
        outputfile.write(tagstring)

    with open('title.txt', 'w', encoding='utf8') as titlefile:
        authors = ", ".join(map(lambda a: get_creator_string(a), data['creator']))
        titlecontent = '---\n'
        titlecontent = titlecontent + 'title: ' + data['name'] + '\n'
        titlecontent = titlecontent + 'author: ' + authors + '\n'
        titlecontent = titlecontent + 'rights: ' + data['license'] + '\n'
        titlecontent = titlecontent + 'language: ' + ", ".join(init_list(data, 'inLanguage')) + '\n'
        titlecontent = titlecontent + '...\n'
        titlefile.write(titlecontent)
